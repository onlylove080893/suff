<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
      protected $table ='articles';
      protected $fillable = ['like'];
	public function getArticleAll()
	{
		// $articles = DB::select('select * from articles');
		// return $articles
            $newArray = array();
            //lay ra id tu bang article_type
            $article_id = DB::table('article_types')
                              ->select('article_types.id')
                              ->get();
            // dd($article_id);
            foreach ($article_id as $key => $value) {
                 $newArray[$value->id] = DB::table('articles')
                  ->join('users', 'users.id', '=', 'articles.user_id')
                  ->join('cities', 'users.city_id', '=', 'cities.id')
                  ->join('article_types','article_types.id','=','articles.article_type_id')
                  ->select('users.first_name as name', 
                               'articles.*',
                               'articles.created_at as day',
                               'articles.id as id',
                               'cities.city_name',
                               'article_types.articles_type')
                  ->where('articles.article_type_id','=',$value->id)
                  ->where('articles.approved', '>', 0)
                  ->where('users.is_blocked','>', 0)
                  ->orderBy('articles.article_type_id', 'desc')
                  ->paginate(3);
            }
           
            return $newArray;
            
	}


      public function showArticle($id)
      {
             $articles = DB::table('articles')
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->join('cities', 'users.city_id', '=', 'cities.id')
            ->join('article_types','article_types.id','=','articles.article_type_id')
            ->select('users.first_name as name', 
                         'articles.*',
                         'articles.created_at as day',
                         'articles.id as id',
                         'cities.city_name',
                         'article_types.articles_type')
            ->where('articles.id', '=', $id)
            ->get();
            
            //C?ng th�m lu?t xem
            $article_view = DB::table('articles')
                  ->select('articles.views')
                  ->where('articles.id','=',$id)
                  ->get();
            DB::table('articles')
            ->where('id', $id)
            ->update(['articles.views' => $article_view[0]->views +1]);

            return $articles;
      }

}
