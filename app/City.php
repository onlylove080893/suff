<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
        public $table = "cities";
        public function user()
        {
            return $this->hasOne('App\User','city_id');
        }
}