<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['article_id','content','user_id'];

    //show comment $id = $article_id
    public function show($id,$take = 5,$skip = 0)
    {
    	$comments = DB::table('comments')
    					->join('users','comments.user_id','=','users.id')
	    	 			->select("comments.*",'users.*','comments.created_at as date_upload')
	    	 			->where('comments.article_id','=', $id)
                        ->orderBy('comments.id', 'desc')
                        ->take($take)
                        ->skip($skip)
	    	 			->get();
	    return $comments;
    }
}
