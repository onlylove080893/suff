<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 18/06/2015
 * Time: 10:24 AM
 */
namespace App\Http\Controllers;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Input;
class ChangePassController extends Controller{
        public function getIndex()
        {
            return View('pages.changepass');
        }
        public function postIndex()
        {
           $user = User::find(208);
           if (User::checkChangePass($user->id,Input::get('current_password'),Input::get('new_password')))
           {
               echo "<script>window.onload = function(){document.getElementById('success').style.display = 'block';};</script>";
               return View('pages.changepass');
           }

           else{
               echo 'Không thành công';
               return View('pages.changepass');
           }
        }
        // Check mật khẩu hiện tại
        public function checkcurrentpass()
        {
            if(User::check_currentpass(Input::get("current_password")))
                return "true";
            else
                return "false";
        }
}
