<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $message    = Input::get('message');
        $article_id = Input::get('article_id');
        $user_id    = Input::get('user_id');

        if($message !== ''){
            $comment = Comment::create([
                'content'    => $message,
                'article_id' => $article_id,
                'user_id'    => $user_id
            ]);

            if($comment){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
        

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {   
        //Lay thong tin get 
        
        $skip = Input::get('skip');
        $take = Input::get('take');
        $article_id = Input::get('article_id');

        if(!$skip){
           $skip = 0;
        }
        if(!$take){
           $take = 5;
        }

        $comment = new Comment;
        $result = $comment->show($article_id,$take,$skip);
       
        return json_encode($result);
        //return Response::json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
