<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('managers.layout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function searchUsers(Request $request){
        $input=$request->search;
        $is_blocked=$request->is_blocked ;
        if(isset($input) && $is_blocked != null){
            $users=User::Filtered($input,$is_blocked)->get();
            return view('managers.searchUser', compact('users'));
        }else{
            return view('managers.searchUser');
        }

    }

    /**
     *Find user then block
     * @param $id
     * @return \Illuminate\View\View
     */
    public function blockUser($id){
        $user=User::findOrFail($id);
        $user->is_blocked = 1;
        $user->save();
        return view('managers.searchUser');
    }

    /**
     * Find user then unlock
     * @param $id
     * @return \Illuminate\View\View
     */
    public function unlockUser($id){
        $user=User::findOrFail($id);
        $user->is_blocked = 0;
        $user->save();
        return view('managers.searchUser');
    }

    public function searchUnApproveArticles(){
        $articles=Article::unApprove()->get();
        return view('managers.approveArticles', compact('articles'));
    }

    public function approveArticle($id){
        $article=Article::find($id);
        $article->update(['approved' => 1]);
//        dd($article);
        return redirect('managers.unApprove');
    }
}
