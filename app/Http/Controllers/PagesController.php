<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Article;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$article = new Article();

		$result = $article->getArticleAll();
		return view('pages.home')->with('data',$result);
	}

	public function show($id)
	{
	}
	public function view()
	{
		return view('pages.view');
	}


}
