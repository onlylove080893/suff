<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\UserRequest;
use App\User;
use App\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(){

        $this->middleware('auth',['except' => 'create']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
//        $input=$request->all();
//        dd($input);
//        User::create($request->all());
//        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user=\Auth::user();
        $cities=City::lists('city_name', 'id');
        return view('users.edit',compact('cities', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param UserRequest $request
     * @return Response
     */
    public function update(UserRequest $request)
    {
        $input=$request->all();

        $fileName = $this->upload($input);
        $input['avatar']=$fileName;

        \Auth::user()->update($input);
        return redirect('home');
    }


    public function represent()
    {
        //lay th�ng tin Get
        $user_id_post = Input::get('user_id_post');
        $user_id_view = Input::get('user_id_view');
        $article_id   = Input::get('article_id');
        $rep          = Input::get('rep');


        //Lay thog tin User post
        $user    = User::find($user_id_post);
        $got_rep  = $user->got_rep;

        //C?ng rep
        $point = $got_rep + $rep;

        $update = $user->update([
              'got_rep' => $point
        ]);


        //Lay thog tin User view
        $user    = User::find($user_id_view);
        $be_rep  = $user->be_rep;

        //C?ng rep
        $point = $be_rep + $rep;

        $update = $user->update([
              'be_rep' => $point
        ]);

        //Lay thog tin Article: like
        $article    = Article::find($article_id);
        $like  = $article->like;

        //C?ng like
        $point = $like + $rep;

        $update = $article->update([
              'like' => $point
        ]);
        

        //ki?m tra c?p nh?p
        if($update){
            echo '1';
        }else{
            echo '0';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }

    public function logout(){
        \Auth::logout();
        return redirect('home');
    }

}
