<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\UserReportArticle;
use App\User;
use App\Article;
use DB;

class UserReportArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('pages.test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

       $title      = Input::get('title');
       $type       = Input::get('type');
       $content    = Input::get('content');
       $article_id = Input::get('article_id');
       $user_id    = Input::get('user_id');

       //Lay thog tin User be_report
       $user    = User::find($user_id);
       $be_reported  = $user->be_reported;


        //C?ng be_report
       if($got_reported ==0){
            $point = $got_reported;
        }else{
            $point = $be_reported - 1;
        }

       $update = $user->update([
          'be_reported' => $point
          ]);

        //lay id user_post
        $article = Article::find($article_id);
        $id_user_post = $article->user_id;

       //Lay thog tin User got_report
        $user    = User::find($id_user_post);
        $got_reported  = $user->got_reported;

        //C?ng get_reported
        $point = $got_reported + 1;

        $update = $user->update([
           'got_reported' => $point
        ]);

       $create = UserReportArticle::create([
            "title"          => $title ,
            "report_type_id" => $type ,
            "reason"         => $content ,
            "user_id"        => $user_id ,
            "article_id"     => $article_id
        ]);

        if($create){
            echo '1';
        }else{
            echo '0';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
