<?php
/**
 * Created by PhpStorm.
 * User: Tan
 * Date: 19/06/2015
 * Time: 6:11 PM
 */
namespace App\Http\Controllers;
use App\Http\Requests;
use App\User;
class ViewInfoController extends Controller{
        public function getIndex()
        {
            $info = User::find(208);
            $cities = User::find($info->id)->city()->get();
            return View('pages.viewinfo')->with('info',$info)->with('cities',$cities);
        }
}