<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required|min:2',
                    'last_name'  => 'required|min:2',
                    'email'      => 'required|email|unique:users',
                    'password'   => 'required|min:3|confirmed',
                    'password_confirmation' => 'required|min:3',
                    'id_card'    => 'required|numeric|min:99999999|unique:users',
                    'phone'      => 'required|numeric|min:99999999',
                    'city_id'    => 'required',
                    'address'    => 'required|min:10',
                    'birthday'   => 'required',
                    'sex'        => 'required|integer|between:0,1'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'first_name' => 'required|min:2',
                    'last_name'  => 'required|min:2',
                    'phone'      => 'required|numeric|min:99999999',
                    'city_id'    => 'required',
                    'address'    => 'required|min:10',
                    'birthday'   => 'required',
                    'sex'        => 'required|integer|between:0,1',
                    'avatar'     => 'required|mimes:png'
                ];
            }
            default:break;
        }
    }

    public function messages(){

        return [
            'id_card.min' => 'Idenity card wrong format',
            'phone.min'   => 'Phone wrong format'
        ];

    }
}
