<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', ['as'=>'home','uses'=>'PagesController@index']);
get('/users/rep',['as'=>'user.rep','uses'=>"UserController@represent"]);

get('/report/',['as' => 'report',"uses" => 'UserReportArticleController@index']);

post('/report/create',['as' => 'report.create',"uses" => 'UserReportArticleController@create']);

get('/articles/{id}',['as'=>'articles.show','uses'=>'ArticleController@show']);

get('/comment',['as'=>'comment.show','uses'=>'CommentController@show']);
post('/comment/create',['as'=>'comment.create','uses'=>'CommentController@create']);

Route::get('/pages/view', ['as' => 'view',"uses" => 'PagesController@view']);

Route::get('managers.unApprove', ['as' => 'unApprove', 'uses' =>'ManagerController@searchUnApproveArticles']);
Route::get('manager.{id}.approveArticle', ['as' => 'approveArticle', 'uses' => 'ManagerController@approveArticle']);
Route::get('managers.searchUsers', ['as'=>'searchUsers','uses' => 'ManagerController@searchUsers']);
Route::get('managers.{id}.blockUser', ['as'=>'blockUser','uses' => 'ManagerController@blockUser']);
Route::get('managers.{id}.unlockUser', ['as'=>'unlockUser','uses' => 'ManagerController@unlockUser']);


Route::post('check','ChangePassController@checkcurrentpass');
Route::controller('changepass','ChangePassController');
Route::get('welcome',function(){
    return View('pages.welcome');
});
Route::controller('view-info','ViewInfoController');
Route::controller('up-news','UpNewsController');

Route::get('users.logout', 'UserController@logout');

Route::resource('home', 'HomeController');

Route::resource('users', 'UserController');

Route::resource('managers', 'ManagerController');

Route::resource('articles', 'ArticleController');

Route::resource('cities', 'CityController');

Route::controllers([
   'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);


Route::get('checkrole', function () {
    $user=\Auth::user();
    if($user->is_blocked == 1){
        \Auth::logout();
        \Session::flash('flash_block','U got blocked');
        return redirect('home');
    }
    if($user->role == 'normal'){
        return redirect('home');
    }
    else if($user->role == 'master'){
        return redirect('managers');
    }
});