<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'birthday',
        'sex',
        'id_card',
        'city_id',
        'phone',
        'address',
        'name',
        'email',
        'password',
        'avatar',
        'be_rep','got_rep','be_reported','got_reported'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['birthday'];

    /**
     *
     * @param $query
     * @param $search
     * @param $is_blocked
     */
    public function scopeFiltered($query,$search,$is_blocked){
        $query->where('email', 'like', '%'.$search.'%')
              ->where('is_blocked', '=', $is_blocked)
              ->where('id', '!=', \Auth::user()->id);
    }

    /**
     * Change birthday format to Carbon
     * @param $date
     */
    public function setBirthdayAttribute($date){
        $this->attributes['birthday'] = Carbon::parse($date);
    }

    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }
    public static function checkChangePass($id, $currentpass, $newpass)
    {
        // Chua r�p du?c giao di?n n�n l?y 1 t�i kho?n b?t k?
        $user = User::find(208);
        if(md5(sha1($currentpass))==$user->password)
        {
           User::where('id','=',$id)->update(array('password' => md5(sha1($newpass))));
           return 1;
        }
        else
        {
           return 0;
        }
    }
    // Ki?m tra m?t kh?u hi?n t?i
    public static function check_currentpass($pass){
        $user = User::find(208);
        if(User::where("password","=",md5(sha1($pass)))->where("id","=",$user->id)->count()>0)
            return true;
        else
            return false;
    }
}
