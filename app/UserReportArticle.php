<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReportArticle extends Model
{
    protected $table    = "user_report_articles";
    protected $fillable = [
					    	'title',
					    	'article_id',
					    	'user_id',
					    	'report_type_id',
					    	'reason'
					    ];
}
