<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->tinyInteger('sex');
            $table->timestamp('birthday');
            $table->string('id_card',20)->unique();
            $table->integer('city_id')->unsigned();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('phone', 20);
            $table->string('address');
            $table->integer('posts')->default(0);
            $table->integer('be_reported')->default(3);
            $table->integer('be_rep')->default(3);
            $table->integer('got_reported')->default(0);
            $table->integer('got_rep')->default(0);
            $table->tinyInteger('is_blocked')->default(0);
            $table->string('role',50)->default('normal');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
