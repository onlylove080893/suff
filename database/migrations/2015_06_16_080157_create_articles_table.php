<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title',200);
            $table->string('image')->nullable();
            $table->string('content', 3000);
            $table->timestamp('closed_at');
            $table->integer('views')->default(0);
            $table->integer('location')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('article_type_id')->unsigned();
            $table->tinyInteger('is_deleted')->default(0);
            $table->tinyInteger('is_closed')->default(0);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('location')->references('id')->on('cities');
            $table->foreign('article_type_id')->references('id')->on('article_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
