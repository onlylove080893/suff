<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedBacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_backs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feed_back_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('content',1000);
            $table->timestamps();

            $table->foreign('feed_back_type_id')->references('id')->on('feed_back_types');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_backs');
    }
}
