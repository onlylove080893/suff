<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

       // $this->call('TestTableSeeder');
        
        //$this->call('CitiesTableSeeder');

        // $this->call('UsersTableSeeder');
        
        // $this->call('CategoriesTableSeeder');
        
      // $this->call('ArticleTypesTableSeeder');

        $this->call('ArticlesTableSeeder');

        Model::reguard();
    }
}

 /* $faker = Faker\Factory::create();

        for($i = 0;$i <10;$i++){
            Article::create{[
                    'title' =>$faker->sentence,
                    'content'=>implode('','$faker->sentences(4)')
                ]};
        }*/
