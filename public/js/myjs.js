/**
 * Created by Tan on 18/06/2015.
 */
$(document).ready(function(){
    $('#form-changepass').validate({
        rules:{
            current_password:{
                required:true,
                remote: {
                    url: "check",
                    type: "post",
                    data: {
                        '_token': function () {
                            return $('input[name="_token"]').val();
                        }
                    }
                }
            },
            new_password:{
                required:true,
                minlength:6
            },
            re_password:{
                equalTo:"#new_password"
            }
        },
        messages:{
            current_password:{
                required:"Vui lòng nhập mật khẩu củ",
                remote:"Mật khẩu không đúng"
            },
            new_password:{
                required:"Vui lòng nhập mật khẩu mới",
                minlength:"Phải nhập 6 ký tự trở lên"
            },
            re_password:{
                equalTo:"Mật khẩu không khớp"
            }
        }
    });
});
