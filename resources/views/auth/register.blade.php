@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            {!! Form::open(['url' => '/auth/register']) !!}
            @include('users.form',['submitButton' => 'Create Accout'])
            {!! Form::close() !!}
        </div>
    </div>

@endsection