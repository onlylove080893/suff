@include ('partials.header') <!-- HEADER -->

	<div class="container body-content">
		
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<span class="glyphicon glyphicon-home span-link" aria-hidden="true"></span>
					@yield('link')
				</div>
			</div>
		</div> <!-- End .row -->
        <!-- Search and content main -->
		<div class="col-md-9 col-xs-9">
			<!-- ====================== form search =============== -->
			<div class="col-md-12 col-xs-12 row-search">
				@include ('partials.search')
			</div><!-- End . row-search -->
            <!-- ====================== Nội dung chính =============== -->
            <div class="col-md-12 col-xs-12 content">

                @yield('content')

            </div><!-- end .content -->   	
		</div>
        <!-- End Search and content main -->
		<!-- =============== Right content =================== -->

		<div class="col-md-3 col-xs-3">
			

			<div class="col-md-12 saibar">
                <div class="list-group">
                        <h3 class="glyphicon glyphicon-user" style="color:blue;font-size: 20px"></h3><span class="list-group-item no-action"> Xin chào !!!</span>
                        <img src="{{asset('public/images/avatar.jpg')}}" alt="Hình đại diện" width="230px" height="220px"><br/><br/>
                        <a class="list-group-item active">Hồ sơ của bạn</a>
                        <a href="{{asset('changepass')}}" class="list-group-item">Đổi mật khẩu</a>
                        <a href="{{asset('view-info')}}" class="list-group-item">Xem thông tin cá nhân</a>
                </div>    
                <!-- Facebook -->
                    <div class="facebook">
                        <div id="fb-root"></div>
                        <script>
                            (function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                        </script>
                        <div class="fb-follow" data-href="https://www.facebook.com/zuck" data-width="200px" data-colorscheme="light" data-layout="standard" data-show-faces="true">

                        </div>
                    </div>
                <!-- End Facebook -->
                    <br>
                    <div class="list-group">
                        <a href="#" class="list-group-item active">Tin xem nhiều nhất</a>
                        <a href="#" class="list-group-item">Item 2</a>
                        <a href="#" class="list-group-item">Item 3</a>
                        <a href="#" class="list-group-item">Item 4</a>
                        <a href="#" class="list-group-item">Item 5</a>
                        <a href="#" class="list-group-item">Item 6</a>
                        <a href="#" class="list-group-item">Item 7</a>
                    </div>
			</div><!--End .saibar-->
		</div>
	</div><!--- body-content -->

	<!-- ==============FOOTER================= -->
@include ('partials.footer')