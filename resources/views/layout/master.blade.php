@include ('partials.header') <!-- HEADER -->

	<div class="container body-content">
		
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<span class="glyphicon glyphicon-home span-link" aria-hidden="true"></span>
					@yield('link')

						<div class="row" style="margin-top:-40px;margin-left: 50%">
							<marquee behavior=""  onmouseover="this.stop()" onmouseout="this.start()"  direction="">
								<span style="color: red">Bài đăng mới:</span>
								<?php 
									$newPost = DB::table('articles')
											     ->select('articles.id','articles.title')
											     ->paginate(3)
											     //->get();
									//dd($newPost);
								 ?>
								 @foreach($newPost as $val)
									<a href="{{route('articles.show', $val->id)}}">{{ $val->title }}</a>
									&nbsp;|&nbsp;
								 @endforeach
							</marquee>
							<!--  {!! $newPost->render() !!} -->
						</div>
				</div>
			</div>
		</div> <!-- End .row -->

		<div class="row">
			<!-- ====================== form search =============== -->
			<div class="col-md-9 col-xs-9 row-search">
				@include ('partials.search')
			</div><!-- End . row-search -->

			<!-- ====================== form Login =============== -->
			<div class="col-md-3 login">
				@include ('partials.login')
			</div> <!-- End .Login -->
		</div>

		<!-- ===============Phan noi dung =================== -->

		<div class="row container-content">
			<div class="col-md-9 content">

				@yield('content')

			</div><!-- end .content -->

			<div class="col-md-3 saibar">
				@yield('saibar')
			</div><!--End .saibar-->
		</div>
	</div><!--- body-content -->

	<!-- ==============FOOTER================= -->
@include ('partials.footer')
