@extends('managers.layout')

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Title</th>
            <th>Create at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($articles))
            @foreach($articles as $article)
                <tr>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td>
                        <a href="{{ action('ManagerController@approveArticle',[$article->id]) }}">Approve</a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
