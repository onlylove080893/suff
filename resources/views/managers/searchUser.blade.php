@extends('managers.layout')

@section('content')

    {!! Form::open(['method' => 'GET' ,'action' => ['ManagerController@searchUsers']]) !!}
        @include('managers.searchForm')
    {!! Form::close() !!}
    @if(isset($users))
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->first_name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <a href="
                            {{ ($user->is_blocked == 0) ?  action('ManagerController@blockUser',
                            [$user->id]) :  action('ManagerController@unlockUser',[$user->id]) }}
                            ">
                            {{ $user->is_blocked == 0 ? 'Blocked' : 'Unlock' }}
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    @endif
@endsection