@extends('layout.afterlogin')
@section('title') Đổi mật khẩu @stop
@section('content')
    <form method="post" action="{{Asset('changepass')}}" id="form-changepass" name="form">
        <h3>Đổi mật khẩu</h3>
        <input type="text" value="Đổi mật khẩu thành công" id="success" class="form-control text-center" disabled/>
        <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Mật khẩu hiện tại"/>
        <p>Vì lý do an ninh, bạn phải xác minh mật khẩu hiện tại trước khi đặt mật khẩu mới.</p>
        <input type="password" id="new_password" name="new_password" class="form-control" placeholder="Mật khẩu mới"/>
        <input type="password" name="re_password" class="form-control" placeholder="Xác nhận mật khẩu mới"/>
        <input type="submit" value="Thực hiện" class="btn btn-block btn-lg btn-primary"/>
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
    </form>
@stop
