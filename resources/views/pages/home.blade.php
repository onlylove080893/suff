@extends('layout.master')

@section('title')
Trang chủ: Thông tin .....
@stop

@section('style')
<!-- có style page thi add vào đây -->
@stop

@section('js')
<!-- có js thì add vào đây -->
@stop

@section('link')
<a href="{{route('home')}}">Trang chủ</a>
@stop

@section('content')
		
	@foreach($data as $value)
		@if(count($value) > 0)
			<div class="row title-content">
				<a href=""><span class="panel-primary" >
					{{ $value[0]->articles_type }}
				</span></a>
			</div>
			@foreach($value as $val)
				<div class="thumbnail">
					<div class="caption post">
						<img src="images/{{ $val->image }}"   src=":)" />
						<a href="{{route('articles.show', $val->id)}}"><h5>{{ $val->title }}</h5></a>
						<span class='address'>{{ $val->city_name }}</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> 
									Ngày đăng: 
									<i>
										<?php 
										$date = date_create($val->created_at);
										echo date_format($date,"d/m/Y");
										?>

									</i>
								</li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>{{ $val->views }}</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>{{ $val->name }}</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>{{ $val->like }}</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->
			@endforeach
				<div class="row">
					<div class="col-md-9">
						
					</div>
				</div>

		@endif
	@endforeach

	@stop <!-- End content -->

	@section('saibar')
	
	<div class="facebook">
		<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-width="200px" data-colorscheme="light" data-layout="standard" data-show-faces="true">

		</div>
	</div>
	<br>
	<div class="list-group">
		<a href="#" class="list-group-item active">Tin xem nhiều nhất</a>
		<a href="#" class="list-group-item">Item 2</a>
		<a href="#" class="list-group-item">Item 3</a>
		<a href="#" class="list-group-item">Item 4</a>
		<a href="#" class="list-group-item">Item 5</a>
		<a href="#" class="list-group-item">Item 6</a>
		<a href="#" class="list-group-item">Item 7</a>
	</div>
	@stop
