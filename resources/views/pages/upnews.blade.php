@extends('layout.afterlogin')
@section('style')
<!-- có style page thi add vào đây -->
@stop

@section('js')
<!-- có js thì add vào đây -->
  <script src="public/js/jquery.validate.js"></script>
  <script src="public/js/myjs.js"></script>
  <script type="text/javascript" src="public/ckeditor/ckeditor.js"></script>
@stop
@section('title') Đăng tin @stop
@section('content')
    <div id="up-news">
    <h3>Đăng tin miễn phí</h3>
    <form action="{{Asset('upnews')}}" method="post" id="form-upnews">
        <label for="" class="stars">Chọn chuyên mục</label><br/>
        <select name="" id="" class="form-control">
            <option value="">1</option>
            <option value="">2</option>
        </select><br/>
        <label for="" class="stars">Tiêu đề tin rao</label><br/>
        <input type="text" placeholder="Nhập tiêu đề tin" class="form-control"> <br/>
        <label for="" class="stars">Chọn tỉnh/thành</label> <br/>
                <select name="" id="" class="form-control">
                    <option value="">Hà Nội</option>
                    <option value="">Huế</option>
                </select><br/>
        <label for="" class="stars">Nội dung</label><br/>
        <textarea class="ckeditor" cols="30" rows="10" name="nd"></textarea><br/>
        <label for="">Chọn hình ảnh</label><br/>
        <label for="" class="btn-success">Tin có ảnh bán nhanh gấp 5 lần</label><br/>
        <input type="file" class="form-control" style="padding-bottom: 45px"/> <br/>
        <center><a href="" class="btn btn-primary">Xem trước</a>
        <input type="submit" value="Hoàn tất" class="btn btn-success"/></center>
    </form>
    </div>
@stop