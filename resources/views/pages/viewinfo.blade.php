@extends('layout.afterlogin')
@section('title') Thông tin cá nhân @stop
@section('content')
    <div id="viewinfo">
        <h3 style="color: #337ab7">Chi tiết cá nhân</h3> <hr/>
       <span>Tên</span>: <label for="">{{$info->first_name}}</label><hr/>
       <span>Họ và tên đệm</span>: <label for="">{{$info->last_name}}</label><hr/>
       <span>Giới tính</span>:
                @if( $info->sex == 1)
                    <label for="">{{'Nam'}}</label>
                @else
                     <label for="">{{'Nữ'}}</label>
                @endif
       <hr/>
       <span>Số lần bị báo cáo</span>: <label>{{$info->got_reported}}</label><hr/>
       <span>Ngày sinh</span>: <label for="">{{date('d-m-Y', strtotime($info->birthday))}}</label><hr/>
       <span>Số Cmnd</span>: <label for="">{{$info->id_card}}</label><hr/>
       <span>Thành phố</span>: <label for="">
                @foreach($cities as $citi)
                     {{$citi->city_name}}
                @endforeach
       </label><hr/>
       <span>Email</span>: <label for="">{{$info->email}}</label><hr/>
       <span>Số điện thoại</span>: <label for="">{{$info->phone}}</label><hr/>
       <span>Địa chỉ</span>: <label for="">{{$info->address}}</label><hr/>
       <span>Ngày đăng ký</span>: <label for="">{{date('d-m-Y', strtotime($info->created_at))}}</label>
    </div>
@stop

