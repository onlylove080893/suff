
	<div class="panel-footer footer">

		<div class="row link-footer">
			<div class="container">
				<ul class="link-left-footer">
					<li>
						<a href="">Trang chủ</a>
						<a href="">Phố đổ củ</a>
						<a href="">Tìm kiếm sản phẩm</a>
					</li>
				</ul>

				<ul class="link-right-footer">
					<li>
						<a href="">Quy định</a>
						<a href="">Quy chế</a>
						<a href="">Hướng dẫn sử dụng</a>
						<a href="">Liên hệ quảng cáo & dịch vụ</a>
					</li>
				</ul>
			</div>
		</div>

		
		<div class="container">
			<div class="row">
			<div class="footerMenu">
				
				<ul class="items level-0">
					<li class="level-1 col-md-3"><a href="">Thời trang nam</a>
						<ul class="items level-2">
							<li class="level-2 node_17"><a href="">Quần áo</a></li>
							<li class="level-2 node_15"><a href="">Giầy dép</a></li>
							<li class="level-2 node_14"><a href="">Đồng hồ và Trang sức</a></li>
							<li class="level-2 node_122"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_16"><a href="">Phụ kiện thời trang</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang nữ</a>
						<ul class="items level-2">
							<li class="level-2 node_50"><a href="">Quần, Áo, Váy</a></li>
							<li class="level-2 node_52"><a href="">Giầy dép</a></li>
							<li class="level-2 node_124"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_49"><a href="">Trang sức và Phụ kiện</a></li>
							<li class="level-2 node_51"><a href="">Nước hoa, Mỹ phẩm</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang cho bé</a>
						<ul class="items level-2">
							<li class="level-2 node_126"><a href="">Cho bé sơ sinh</a></li>
							<li class="level-2 node_127"><a href="">Cho bé gái</a></li>
							<li class="level-2 node_128"><a href="">Cho bé trai</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Điện thoại - Máy tính bảng</a>
						<ul class="items level-2">
							<li class="level-2 node_56"><a href="">SmartPhone, Cao cấp</a></li>
							<li class="level-2 node_94"><a href="">Điện thoại bình dân</a></li>
							<li class="level-2 node_58"><a href="">Máy tính bảng, Máy đọc sách</a></li>
							<li class="level-2 node_57"><a href="">Linh kiện, Phụ kiện</a></li>
						</ul>
					</li>
				</ul>

			</div>
			</div>
		</div> <!-- End Container -->

		<div class="container">
			<div class="row">
			<div class="footerMenu">
				
				<ul class="items level-0">
					<li class="level-1 col-md-3"><a href="">Thời trang nam</a>
						<ul class="items level-2">
							<li class="level-2 node_17"><a href="">Quần áo</a></li>
							<li class="level-2 node_15"><a href="">Giầy dép</a></li>
							<li class="level-2 node_14"><a href="">Đồng hồ và Trang sức</a></li>
							<li class="level-2 node_122"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_16"><a href="">Phụ kiện thời trang</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang nữ</a>
						<ul class="items level-2">
							<li class="level-2 node_50"><a href="">Quần, Áo, Váy</a></li>
							<li class="level-2 node_52"><a href="">Giầy dép</a></li>
							<li class="level-2 node_124"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_49"><a href="">Trang sức và Phụ kiện</a></li>
							<li class="level-2 node_51"><a href="">Nước hoa, Mỹ phẩm</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang cho bé</a>
						<ul class="items level-2">
							<li class="level-2 node_126"><a href="">Cho bé sơ sinh</a></li>
							<li class="level-2 node_127"><a href="">Cho bé gái</a></li>
							<li class="level-2 node_128"><a href="">Cho bé trai</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Điện thoại - Máy tính bảng</a>
						<ul class="items level-2">
							<li class="level-2 node_56"><a href="">SmartPhone, Cao cấp</a></li>
							<li class="level-2 node_94"><a href="">Điện thoại bình dân</a></li>
							<li class="level-2 node_58"><a href="">Máy tính bảng, Máy đọc sách</a></li>
							<li class="level-2 node_57"><a href="">Linh kiện, Phụ kiện</a></li>
						</ul>
					</li>
				</ul>

			</div>
			</div>
		</div> <!-- End Container -->
	</div><!-- End .footer -->


	<div class="backtop" id='backtop' >
		<img src="{!!Asset('images/back-top.png')!!}" alt="">
	</div>
	<div class="loading-load">	
		<img src="{!!Asset('images/loading-page.gif')!!}" class="img-responsive" alt="Image">
	</div>
	<div class="col-md-2 chat" id='chat-online'>	
		<div class="body-chat">
			<div class="header-chat">Hổ trợ online
			<span class="faded" onclick="fadeIn()" >X</span>

			</div>
			<div class="body-content-chat">
				<div class="content-chat">
					<form action="" method="POST" role="form">
						
						<div class="form-group">
							<label for="email" class="control-label">Vui lòng nhập thông tin của bạn!</label>
							<input type="text" name="email" class="form-control" id="email" placeholder="Email của bạn">
						</div>
						<div class="form-group">
							<input type="text" name="name" class="form-control" id="name" placeholder="Tên đầy đủ của bạn.">
						</div>
						<div class="form-group">
							<input type="text" name="phone" class="form-control" id="phone" placeholder="Số phone của bạn">
						</div>
					
						<button type="submit" class="btn btn-primary">Gửi tin nhắn</button>
					</form>
				</div>
				<div class="footer-chat">Green global</div>
			</div>
		</div>
	</div>

	<script>
		function fadeIn() {
			
			$(".backtop").removeClass('backtop').addClass('backtopIn');

			$('.body-content-chat').css('display', 'block').fadeIn(200, function() {

				$('.faded').removeAttr('onclick').attr('onclick', 'fadeOut()');
				$('#chat-online').removeClass('col-md-2').addClass('col-md-3');
				
			});;
		}
		function fadeOut() {

			$(".backtopIn").removeClass('backtopIn').addClass('backtop');

			$('.body-content-chat').css('display','none').fadeOut(200, function() {

				$('.faded').removeAttr('onclick').attr('onclick', 'fadeIn()');
				$('#chat-online').removeClass('col-md-3').addClass('col-md-2');
				
			});
		}
	</script>
</body>
</html>

