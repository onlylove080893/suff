<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <!-- Bootstrap CSS -->
  <link href="{!!Asset('css/jquery-ui.min.css')!!}" rel="stylesheet">
  <link href="{!!Asset('css/bootstrap.min.css')!!}" rel="stylesheet">
  <link href="{!!Asset('css/app.css')!!}" rel="stylesheet">
  @yield('style')

  <!-- jQuery -->
  <script src="{!!Asset('js/jquery.js')!!}"></script>
  <script src="{!!Asset('js/jquery-ui.min.js')!!}"></script>
  <!-- Bootstrap JavaScript -->
  <script src="{!!Asset('js/bootstrap.min.js')!!}"></script>
  <script src="{!!Asset('js/back-top.js')!!}"></script>
  @yield('js')

</head>
<body>

  <!-- ====================== Navbar =============== -->
  <nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
    </div><!--navbar-header-->
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="{{route('home')}}" class="dropdown-toggle" data-toggle="dropdown">Thời trang</a>
          <ul class="dropdown-menu">
            <li><a href="#">Viet Nam</a></li>
            <li><a href="#">English</a></li>
            <li><a href="#">Japanese</a></li>
            <li><a href="#">Chinese</a></li>
          </ul>

        </li>
        <li><a href="#">Công Nghệ</a></li>
        <li><a href="#">Dịch vụ</a></li>
        <li><a href="#">Mua sắm</a></li>
        <li><a href="#">Phố đồ cũ</a></li>
        <li><a href="#">Liên hệ</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="up"><a href="#">Đăng tin</a></li>
        <li><a href="#">Đăng Nhập</a></li>
        <li><a href="#">Đăng Ký</a></li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Language <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">Viet Nam</a></li>
            <li><a href="#">English</a></li>
            <li><a href="#">Japanese</a></li>
            <li><a href="#">Chinese</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav><!--- .navbar -->
