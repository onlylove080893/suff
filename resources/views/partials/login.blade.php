<form action="#" method="POST" role="form">
	<legend>Đăng nhập tài khoản</legend>
	<input type="hidden" class="_token" name="_token" value="{{csrf_token()}}">
	<div class="form-group">
		<label for="">Username:</label>
		<input type="text" class="form-control username" name ="username" id="username" placeholder="Tài khoản">
		<label for="">Password:</label>
		<input type="password" class="form-control password" name ="password" id="password" placeholder="Mật khẩu">
	</div>
	<button type="submit" class="btn btn-primary">Đăng nhập</button>
	<a href="">Đăng ký</a>
</form>