<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/back-top.js"></script>

</head>
<body>
<!-- ====================== Navbar =============== -->
<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Thời trang</a>
                <ul class="dropdown-menu">
                    <li><a href="#">Viet Nam</a></li>
                    <li><a href="#">English</a></li>
                    <li><a href="#">Japanese</a></li>
                    <li><a href="#">Chinese</a></li>
                </ul>

            </li>
            <li><a href="#">Công Nghệ</a></li>
            <li><a href="#">Dịch vụ</a></li>
            <li><a href="#">Mua sắm</a></li>
            <li><a href="#">Phố đồ cũ</a></li>
            <li><a href="#">Liên hệ</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="up"><a href="#">Đăng tin</a></li>
            <li><a href="#">Đăng Nhập</a></li>
            <li><a href="#">Đăng Ký</a></li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Language <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Viet Nam</a></li>
                    <li><a href="#">English</a></li>
                    <li><a href="#">Japanese</a></li>
                    <li><a href="#">Chinese</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
<!-- ====================== form search =============== -->
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <span class="glyphicon glyphicon-home" style="font-size: 25px;color:blue;top:5px;padding:0px;margin:0 10px" aria-hidden="true"></span>
                <a href="#">Trang chủ</a> >>
                <a href="#">Phố đồ cũ</a> >>
                <a href="#">Tìm kiếm</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-xs-9 row-search">
            <form class="navbar-form navbar-left" role="search">
                <input type="text" class="form-control sr-text" placeholder="Hôm nay bạn muốn mua gì?">
                <div class="form-group search">
                    <input type="text" class="form-control" placeholder="Tất cả các địa điểm">
                    <input type="text" class="form-control" placeholder="Ngày đăng">
                    <input type="text" class="form-control" placeholder="Tất cả danh mục">
                    <input type="text" class="form-control" placeholder="Sản phẩm">

                </div>

                <div class="btn-search">
                    <button type="submit" class="btn btn-danger">Tìm kiếm</button>
                    <a href="#" class="btn btn-warning" style="margin-left:20px; width: 25% !important;">Đăng tin</a>
                </div>
            </form>
        </div>
        <!-- ====================== form Login =============== -->
        <div class="col-md-3 login">

            <form action="#" method="POST" role="form">
                <legend>Đăng nhập tài khoản</legend>
                <div class="form-group">
                    <label for="">Username:</label>
                    <input type="text" class="form-control username" name ="username" id="username" placeholder="Tài khoản">
                    <label for="">Password:</label>
                    <input type="password" class="form-control password" name ="password" id="password" placeholder="Mật khẩu">
                </div>
                <button type="submit" class="btn btn-primary">Đăng nhập</button>
                <a href="">Đăng ký</a>
            </form>
        </div>
    </div>

    <!-- ===============Phan noi dung =================== -->
    <div class="row">
        <hr/>

        <div class="col-md-9 content">
            <!-- =============Noi dung post/search ============ -->
            <div class="row title-content">
                <a href=""><span class="panel-primary" >Máy tính, thiết bị văn phòng</span></a>
            </div>
            <div class="thumbnail">

                <img data-src="#" alt="">
                <div class="caption post">
                    <img src="images/01.jpg" />
                    <a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
                    <span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
                    <div class="col-md-5 info">
                        <ul>
                            <li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
                            <li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
                        </ul>
                    </div>

                    <div class="col-md-5 info2">
                        <ul>
                            <li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
                            <li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end thumbnail -->
            <!-- =============End Noi dung post/search ============ -->

            <!-- =============Noi dung post/search ============ -->

            <div class="thumbnail">

                <img data-src="#" alt="">
                <div class="caption post">
                    <img src="images/01.jpg" />
                    <a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
                    <span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
                    <div class="col-md-5 info">
                        <ul>
                            <li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
                            <li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
                        </ul>
                    </div>

                    <div class="col-md-5 info2">
                        <ul>
                            <li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
                            <li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end thumbnail -->
            <!-- =============End Noi dung post/search ============ -->

            <!-- =============Noi dung post/search ============ -->
            <div class="thumbnail">

                <img data-src="#" alt="">
                <div class="caption post">
                    <img src="images/01.jpg" />
                    <a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
                    <span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
                    <div class="col-md-5 info">
                        <ul>
                            <li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
                            <li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
                        </ul>
                    </div>

                    <div class="col-md-5 info2">
                        <ul>
                            <li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
                            <li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end thumbnail -->
            <!-- =============End Noi dung post/search ============ -->

            <!-- =============Noi dung post/search ============ -->
            <div class="row title-content">
                <a href=""><span>Điện thoại - Máy tính</span></a>
            </div>
            <div class="thumbnail">

                <img data-src="#" alt="">
                <div class="caption post">
                    <img src="images/01.jpg" />
                    <a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
                    <span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
                    <div class="col-md-5 info">
                        <ul>
                            <li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
                            <li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
                        </ul>
                    </div>

                    <div class="col-md-5 info2">
                        <ul>
                            <li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
                            <li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end thumbnail -->

            <div class="thumbnail">

                <img data-src="#" alt="">
                <div class="caption post">
                    <img src="images/01.jpg" />
                    <a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
                    <span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
                    <div class="col-md-5 info">
                        <ul>
                            <li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
                            <li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
                        </ul>
                    </div>

                    <div class="col-md-5 info2">
                        <ul>
                            <li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
                            <li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end thumbnail -->

            <!-- =============End Noi dung post/search ============ -->

        </div><!-- end .content -->



        <!-- ====================== saibar =============== -->
        <div class="col-md-3 saibar">
            <div class="facebook">
                <div id="fb-root"></div>
                <script>
                    (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <div class="fb-follow" data-href="https://www.facebook.com/zuck" data-width="200px" data-colorscheme="light" data-layout="standard" data-show-faces="true">

                </div>
            </div>
            <br>
            <div class="list-group">
                <a href="#" class="list-group-item active">Tin xem nhiều nhất</a>
                <a href="#" class="list-group-item">Item 2</a>
                <a href="#" class="list-group-item">Item 3</a>
                <a href="#" class="list-group-item">Item 4</a>
                <a href="#" class="list-group-item">Item 5</a>
                <a href="#" class="list-group-item">Item 6</a>
                <a href="#" class="list-group-item">Item 7</a>
            </div>

        </div>
    </div>
</div>

<!-- ====================== Footer =============== -->
<div class="panel-footer footer">
    <div class="row">
	  			<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
	  			 Libero quis explicabo eveniet perferendis ut natus quaerat excepturi
	  			  inventore modi recusandae quo officia repellat qui, dolore architecto
	  			  saepe, enim quam corrupti aperiam obcaecati? Aliquam aperiam quibusdam
	  			  quasi quia porro dolore temporibus nesciunt iure perspiciatis aliquid dolorum,
	  			   veniam assumenda eum ex itaque ab accusantium delectus asperiores dicta ut!
	  			   Obcaecati ipsam temporibus laborum fuga, repellendus, libero, harum illo iusto
	  			   aperiam consectetur hic architecto, voluptatum eveniet dolorum ducimus sapiente
	  			    rerum culpa. Alias nihil reprehenderit vel nisi. Dolorum a delectus sequi porro
	  			    fuga, quis, enim. Beatae consectetur numquam magnam facere veniam quisquam, fuga
	  			    deserunt inventore!</span>
    </div>

</div><!-- End .footer -->
<div id="backtop">
    <img src="images/back-top.png" alt="">
</div>
</body>
</html>