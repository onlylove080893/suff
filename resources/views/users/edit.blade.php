@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            {!! Form::model($user,['method' => 'PATCH','action' => ['UserController@update',$user->id],'files'=>true]) !!}
                @include('users.form',['submitButton' => 'Edit Account', 'birthday' => $user->birthday->format('Y-m-d')])
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    </div>
@endsection

