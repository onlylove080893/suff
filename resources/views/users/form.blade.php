<div class="form-group">
    {!! Form::label('firstName',"First Name :") !!}
    {!! Form::text('first_name',null,['class' => 'form-control']) !!}
</div>
@if($errors->first('first_name')) <p style="color:red">{{ $errors->first('first_name') }}</p> @endif

<div class="form-group">
    {!! Form::label('lastName',"Last Name :") !!}
    {!! Form::text('last_name',null,['class' => 'form-control']) !!}
</div>
@if($errors->first('last_name')) <p style="color:red">{{ $errors->first('last_name') }}</p> @endif

<div class="form-group">
    {!! Form::label('sex',"Sex :") !!}
    {!! Form::select('sex',array_merge(array( null => 'Please Select'),[0 =>'Female',1 =>'Male']),null, ['class' => 'form-control']) !!}
</div>
@if($errors->first('sex')) <p style="color:red">{{ $errors->first('sex') }}</p> @endif

<div class="form-group">
    {!! Form::label('avatar',"Avatar :") !!}
    {!! Form::file('avatar', ['class' => 'form-control']) !!}
</div>

<?php $date=(isset($birthday)? $birthday : date('Y-m-d')); ?>
<div class="form-group">
    {!! Form::label('birthday',"Birthday :") !!}
    {!! Form::input('date', 'birthday',$date, ['class' => 'form-control']) !!}
</div>
@if($errors->first('birthday')) <p style="color:red">{{ $errors->first('birthday') }}</p> @endif


<div class="form-group">
    {!! Form::label('phone',"Phone Number :") !!}
    {!! Form::text('phone',null,['class' => 'form-control']) !!}
</div>
@if($errors->first('phone')) <p style="color:red">{{ $errors->first('phone') }}</p> @endif

<div class="form-group">
    {!! Form::label('city_id','City :') !!}
    {!! Form::select('city_id',$cities,null,['class' => 'form-control',]) !!}
</div>

<div class="form-group">
    {!! Form::label('address',"Address :") !!}
    {!! Form::text('address',null,['class' => 'form-control',]) !!}
</div>
@if($errors->first('address')) <p style="color:red">{{ $errors->first('address') }}</p> @endif

<div class="form-group">
    {!! Form::submit($submitButton,['class' => 'btn btn-primary form-control']) !!}
</div>