@extends('layout')
@section('content')
    <hr>
    @if($users)
    @foreach($users as $user)
        <h3><a href="{{ action('UserController@edit',[$user->id]) }}">{{'hello :'.$user->email  }}</a></h3>
        {!! Form::open(['method' => 'DELETE','action' => ['UserController@destroy',
                        $user->id],'class' => 'pull-left']) !!}
            {!! Form::submit('delete',['class' => 'btn btn-danger form-control']) !!}
        {!! Form::close() !!}
    @endforeach
    @endif

@endsection

